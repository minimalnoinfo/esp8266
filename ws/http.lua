do
  srv = net.createServer(net.TCP)
srv:listen(80, function(conn)
    conn:on("receive", function(sck, payload)
        local _, e, method = string.find(payload, "([A-Z]+) /[^\r]* HTTP/%d%.%d\r\n")
		
		print("payload: " .. payload) 
		print("method: " .. method) 
		print(payload)
		
        sck:send("HTTP/1.0 200 OK\r\nContent-Type: text/html\r\n\r\n<h1> Hello, NodeMCU.</h1>")
    end)
    conn:on("sent", function(sck) sck:close() end)
end)


end
