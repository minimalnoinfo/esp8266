websocket.createServer(80, function (socket)
  local data
  node.output(function (msg)
    return socket.send(msg, 1)
  end, 1)
  print("New websocket client connected")
  function socket.onmessage(payload, opcode)
  print(payload)
  print(opcode)
    if opcode == 1 then
      if payload == "ls" then
        print("dir")
        local list = file.list()
        local lines = {}
        for k, v in pairs(list) do
          lines[#lines + 1] = k .. "\0" .. v
        end
        payload = table.concat(lines, "\0")
        socket.send(table.concat(lines, "\0"), 2)
        return
      end
      local command, name = payload:match("^([a-z]+):(.*)$")
      print("command:" .. command)
      print("name:" .. name)
      if command == "gpioon" then
        gpio.mode(name, gpio.OUTPUT)
        gpio.write(name, gpio.HIGH)
      elseif command == "gpiooff" then
        gpio.mode(name, gpio.OUTPUT)
        gpio.write(name, gpio.LOW)
      elseif command == "load" then
        file.open(name, "r")
        socket.send(file.read(), 2)
        file.close()
      elseif command == "save" then
        file.open(name, "w")
        file.write(data)
        data = nil
        file.close()
      elseif command == "compile" then
        node.compile(name)
      elseif command == "run" then
        dofile(name)
      elseif command == "eval" then 
        local fn = loadstring(name)
        data = nil
        fn()
      else
        print("Invalid command")
      end
    elseif opcode == 2 then
      data = payload
    end
  end
end)
